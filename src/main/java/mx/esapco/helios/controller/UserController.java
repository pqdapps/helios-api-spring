package mx.esapco.helios.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mx.esapco.helios.domain.context.DomainContextHolder;
import mx.esapco.helios.domain.dto.UserRequestDTO;
import mx.esapco.helios.domain.resource.UserResource;
import mx.esapco.helios.persistence.app.entity.User;
import mx.esapco.helios.service.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    private ConversionService conversionService;

    @Autowired
    public UserController(UserService userService, ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @GetMapping
    // @RequestMapping(value = "", method = RequestMethod.GET)
    // @ResponseBody
    public ResponseEntity<List<UserResource>> getUsers(
            @RequestParam(name = "domain", defaultValue = "") String domain) {
        DomainContextHolder.setDomain(domain);
        List<User> users = userService.getUsers();
        List<UserResource> result = new ArrayList<>();

        for (User user : users) {
            // result.add(convertToUserResource(user));
            result.add(conversionService.convert(user, UserResource.class));
        }

        return ResponseEntity.ok(result);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserResource createUser(@RequestBody UserRequestDTO request) {
        // User user = convertUserRequestDtoToUser(request);
        User user = conversionService.convert(request, User.class);
        user = userService.createUser(user);
        // return convertToUserResource(user);
        return conversionService.convert(user, UserResource.class);
    }

    // private User convertUserRequestDtoToUser(UserRequestDTO request) {
//        // @formatter:off
//        User user = new User();
//        user.setEmail(request.getEmail());
//        user.setEnabled(true);
//        user.setExpired(false);
//        user.setFirstName(request.getFirstName());
//        user.setLastName(request.getLastName());
//        user.setLocked(false);
//        user.setPassword(request.getPassword());
//        user.setPasswordExpired(false);
//        user.setUserName(request.getUserName());
//        
//        if (request.getRoles() != null 
//                && !request.getRoles().isEmpty()) {
//            Set<Role> roles = new HashSet<>();
//            for (String userRole : request.getRoles()) {
//                Role role = new Role();
//                role.setRoleName(userRole);
//                roles.add(role);
//            }
//            
//            user.setRoles(roles);
//        }
//        
//        return user;
//        // @formatter:on
    // }

    // private UserResource convertToUserResource(User user) {
//        // @formatter:off
// 
//        UserResourceBuilder resourceBuilder = UserResource.builder()
//                .id(user.getUserId())
//                .email(user.getEmail())
//                .firstName(user.getFirstName())
//                .lastName(user.getLastName())
//                .userName(user.getUserName());
//        if (user.getRoles() != null) {
//            resourceBuilder.roles(user.getRoles().stream().map(r -> r.getRoleName()).collect(Collectors.toList()));
//            //.roles(convertRolesToString(user.getRoles()))
//            //.roles("ADMIN", "USER")
//        }
//                
//        return resourceBuilder.build();
//     // @formatter:on
    // }

    // private List<String> convertRolesToString(Set<Role> roles) {
    // if (roles == null) {
    // return Collections.emptyList();
    // }
    // List<String> result = new ArrayList<>(roles.size());
    // for (Role role : roles) {
    // result.add(role.getRoleName());
    // }
    //
    // return result;
    // }

}
