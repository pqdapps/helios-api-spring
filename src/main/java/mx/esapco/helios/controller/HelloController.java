package mx.esapco.helios.controller;

import java.util.Collections;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hello")
public class HelloController {

    @GetMapping
    // @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> hello() {
        return ResponseEntity.ok(Collections.singletonMap("message", "Hello World!"));
    }

}
