package mx.esapco.helios;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Clase que registra todos los filtros basicos de seguridad en el
 * FilterChainProxy de Spring Security. Al extender
 * {@code AbstractSecurityWebApplicationInitializer} Spring la detecta
 * automaticamente y ejecuta todas las funciones dentro de la clase abstracta,
 * por lo tanto no es necesario sobre-escribir sus metodos.
 * 
 * @author djimenez
 *
 */
public class HeliosSecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {

}
