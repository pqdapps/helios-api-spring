package mx.esapco.helios.persistence.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.esapco.helios.persistence.management.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    Company findByName(String name);

    @Query("SELECT c FROM Company c WHERE c.name = :name AND c.active = 1")
    Company findByNameAndActive(@Param("name") String name);
}
