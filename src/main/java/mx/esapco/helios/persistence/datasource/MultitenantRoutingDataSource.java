package mx.esapco.helios.persistence.datasource;

import javax.sql.DataSource;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import mx.esapco.helios.security.authentication.DomainUsernamePasswordAuthenticationToken;
import mx.esapco.helios.security.core.userdetails.HeliosUserDetails;

public class MultitenantRoutingDataSource extends DynamicRoutingDataSource {

    public MultitenantRoutingDataSource(DataSourceProvider dataSourceProvider,
            DataSource defaultDataSource) {
        super(dataSourceProvider, defaultDataSource);
    }

    @Override
    protected Object determineCurrentLookupKey() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        if (authentication instanceof DomainUsernamePasswordAuthenticationToken) {
            return ((DomainUsernamePasswordAuthenticationToken) authentication).getDomain();
        }
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken userPwdToken = (UsernamePasswordAuthenticationToken) authentication;
            String username = userPwdToken.getName();
            if (username == null) {
                return null;
            }
            String[] userDomain = username.split("@");
            if (userDomain.length == 1) {
                return null;
            }
            return userDomain[1];
        }
        if (authentication instanceof OAuth2Authentication) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof HeliosUserDetails) {
                return ((HeliosUserDetails) principal).getCompanyName();
            }
            if (principal instanceof String) {
                String[] userDomain = ((String) principal).split("@");
                if (userDomain.length == 1) {
                    return null;
                }
                return userDomain[1];
            }
        }
        if (authentication instanceof HeliosUserDetails) {
            return ((HeliosUserDetails) authentication).getCompanyName();
        }
        return null;
    }

}
