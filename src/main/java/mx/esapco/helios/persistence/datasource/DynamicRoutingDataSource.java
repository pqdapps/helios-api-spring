package mx.esapco.helios.persistence.datasource;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.AbstractDataSource;
import org.springframework.util.Assert;

public abstract class DynamicRoutingDataSource extends AbstractDataSource {

    private final DataSourceProvider dataSourceProvider;
    private final DataSource defaultDataSource;

    public DynamicRoutingDataSource(final DataSourceProvider dataSourceProvider,
            final DataSource defaultDataSource) {
        if (dataSourceProvider == null) {
            throw new IllegalArgumentException("dataSourceProvider can't be null");
        }
        if (defaultDataSource == null) {
            throw new IllegalArgumentException("defaultDataSource can't be null");
        }
        this.dataSourceProvider = dataSourceProvider;
        this.defaultDataSource = defaultDataSource;
    }

    protected abstract Object determineCurrentLookupKey();

    protected DataSource determineTargetDataSource() {
        Assert.notNull(this.dataSourceProvider, "dataSourceProvider not initialized");
        Object lookupKey = determineCurrentLookupKey();
        if (lookupKey == null) {
            return defaultDataSource;
        }
        DataSource dataSource = this.dataSourceProvider.getDataSource(lookupKey);
        if (dataSource == null) {
            dataSource = this.defaultDataSource;
        }
        if (dataSource == null) {
            throw new IllegalStateException(
                    "Cannot determine target DataSource for lookup key [" + lookupKey + "]");
        }
        return dataSource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return determineTargetDataSource().getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return determineTargetDataSource().getConnection(username, password);
    }

}
