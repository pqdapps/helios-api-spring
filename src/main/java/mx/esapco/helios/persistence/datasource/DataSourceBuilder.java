package mx.esapco.helios.persistence.datasource;

import javax.sql.DataSource;

public interface DataSourceBuilder<T> {

    public DataSource build(T dataSourceDetails);
}
