package mx.esapco.helios.persistence.datasource;

import javax.sql.DataSource;

public interface DataSourceProvider {

    DataSource getDataSource(Object key);

}
