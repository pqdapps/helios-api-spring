package mx.esapco.helios.persistence.datasource;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import mx.esapco.helios.persistence.management.entity.Company;

@Component
public class HikariDataSourceBuilder implements DataSourceBuilder<Company> {

    private Environment environment;

    @Autowired
    public HikariDataSourceBuilder(Environment environment) {
        this.environment = environment;
    }

    @Override
    public DataSource build(Company dataSourceDetails) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(dataSourceDetails.getUrl());
        hikariConfig.setUsername(dataSourceDetails.getUserName());
        hikariConfig.setPassword(dataSourceDetails.getPassword());
        hikariConfig.setDriverClassName(dataSourceDetails.getDriverClass());
        hikariConfig.setPoolName(dataSourceDetails.getName());
        hikariConfig
                .setConnectionTestQuery(environment.getProperty("app.datasource.validation-query"));
        hikariConfig.setMinimumIdle(
                environment.getProperty("app.datasource.connection-pool.min-idle", Integer.class));
        hikariConfig.setMaximumPoolSize(
                environment.getProperty("app.datasource.connection-pool.max-idle", Integer.class));
        hikariConfig.setIdleTimeout(
                environment.getProperty("app.datasource.connection-pool.idle-timeout", Long.class));
        hikariConfig.setConnectionTimeout(
                environment.getProperty("app.datasource.connection-timeout", Long.class));
        hikariConfig
                .setReadOnly(environment.getProperty("app.datasource.read-only", Boolean.class));
        return new HikariDataSource(hikariConfig);
    }

}
