package mx.esapco.helios.persistence.datasource;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.esapco.helios.persistence.management.entity.Company;
import mx.esapco.helios.persistence.management.repository.CompanyRepository;

@Component
public class DatabaseDataSourceProvider implements DataSourceProvider {

    private CompanyRepository companyRepository;
    private DataSourceBuilder<Company> dataSourceBuilder;

    @Autowired
    public DatabaseDataSourceProvider(CompanyRepository companyRepository,
            DataSourceBuilder<Company> datasourceBuilder) {
        this.companyRepository = companyRepository;
        this.dataSourceBuilder = datasourceBuilder;
    }

    @Override
    public DataSource getDataSource(Object key) {
        if (key == null) {
            throw new IllegalArgumentException("The key can't be null or empty");
        }
        if (!key.getClass().isAssignableFrom(String.class)) {
            throw new IllegalArgumentException("The key is expected to be of type String");
        }
        Company heliosCompany = companyRepository.findByNameAndActive((String) key);
        if (heliosCompany != null) {
            return dataSourceBuilder.build(heliosCompany);
        }
        return null;
    }
}
