package mx.esapco.helios.persistence.datasource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

public class CachedDataSourceProvider implements DataSourceProvider {

    private final DataSourceProvider delegate;

    private final Map<Object, DataSource> dataSourceCache = new ConcurrentHashMap<>();

    public CachedDataSourceProvider(DataSourceProvider delegate) {
        this.delegate = delegate;
    }

    @Override
    public DataSource getDataSource(Object key) {
        if (key == null) {
            return null;
        }
        if (!dataSourceCache.containsKey(key)) {
            DataSource dataSource = delegate.getDataSource(key);
            if (dataSource != null) {
                dataSourceCache.put(key, dataSource);
            }
        }
        return dataSourceCache.get(key);
    }

}
