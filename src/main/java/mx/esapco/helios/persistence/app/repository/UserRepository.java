package mx.esapco.helios.persistence.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.esapco.helios.persistence.app.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    // JPQL (Java Persistence Query Language)
    @Query("SELECT u FROM User u WHERE u.userName = :name")
    public User findByUserName(@Param("name") String userName);

    // Equivalent
    // public User findByUserName(String userName);

}
