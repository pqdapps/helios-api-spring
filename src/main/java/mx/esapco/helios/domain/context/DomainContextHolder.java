package mx.esapco.helios.domain.context;

public class DomainContextHolder {

    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<String>();

    public static void setDomain(String domain) {
        if (domain == null) {
            throw new IllegalArgumentException("domain can't be null");
        }

        CONTEXT_HOLDER.set(domain);
    }

    public static String getDomain() {
        return CONTEXT_HOLDER.get();
    }

    public static void clearContext() {
        CONTEXT_HOLDER.remove();
    }
}
