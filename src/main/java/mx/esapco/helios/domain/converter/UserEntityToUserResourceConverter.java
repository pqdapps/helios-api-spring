package mx.esapco.helios.domain.converter;

import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;

import mx.esapco.helios.domain.resource.UserResource;
import mx.esapco.helios.domain.resource.UserResource.UserResourceBuilder;
import mx.esapco.helios.persistence.app.entity.User;

public class UserEntityToUserResourceConverter implements Converter<User, UserResource> {

    @Override
    public UserResource convert(User source) {
     // @formatter:off
        
        UserResourceBuilder userResourceBuilder = UserResource.builder()
                .id(source.getUserId())
                .email(source.getEmail())
                .firstName(source.getFirstName())
                .lastName(source.getLastName())
                .userName(source.getUserName());
        if (source.getRoles() != null) {
            userResourceBuilder.roles(source.getRoles().stream().map(r -> r.getRoleName()).collect(Collectors.toList()));
            //.roles(convertRolesToString(user.getRoles()))
        }
                
        return userResourceBuilder.build();
     // @formatter:on
    }

    // private List<String> convertRolesToString(Set<Role> roles) {
    // if (roles == null) {
    // return Collections.emptyList();
    // }
    // List<String> result = new ArrayList<>(roles.size());
    // for (Role role : roles) {
    // result.add(role.getRoleName());
    // }
    //
    // return result;
    // }

}
