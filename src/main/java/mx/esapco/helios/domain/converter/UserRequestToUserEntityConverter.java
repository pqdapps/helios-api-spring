package mx.esapco.helios.domain.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.core.convert.converter.Converter;

import mx.esapco.helios.domain.dto.UserRequestDTO;
import mx.esapco.helios.persistence.app.entity.Role;
import mx.esapco.helios.persistence.app.entity.User;

public class UserRequestToUserEntityConverter implements Converter<UserRequestDTO, User> {

    @Override
    public User convert(UserRequestDTO source) {
     // @formatter:off
        User user = new User();
        user.setEmail(source.getEmail());
        user.setEnabled(true);
        user.setExpired(false);
        user.setFirstName(source.getFirstName());
        user.setLastName(source.getLastName());
        user.setLocked(false);
        user.setPassword(source.getPassword());
        user.setPasswordExpired(false);
        user.setUserName(source.getUserName());
        
        if (source.getRoles() != null 
                && !source.getRoles().isEmpty()) {
            Set<Role> roles = new HashSet<>();
            for (String userRole : source.getRoles()) {
                Role role = new Role();
                role.setRoleName(userRole);
                roles.add(role);
            }
            
            user.setRoles(roles);
        }
        
        return user;
        // @formatter:on
    }

}
