package mx.esapco.helios.domain.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//@JsonInclude(value = Include.NON_NULL)
public class UserResource {

    private Long id;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private List<String> roles;

    private UserResource() {

    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the roles
     */
    public List<String> getRoles() {
        return roles;
    }

    /**
     * @param roles
     *            the roles to set
     */
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public static UserResourceBuilder builder() {
        return new UserResourceBuilder();
    }

    public static class UserResourceBuilder {

        private Long id;
        private String userName;
        private String firstName;
        private String lastName;
        private String email;
        private List<String> roles;

        public UserResourceBuilder() {
        }

        public UserResourceBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public UserResourceBuilder userName(String userName) {
            this.userName = userName;
            return this;
        }

        public UserResourceBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserResourceBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserResourceBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserResourceBuilder roles(List<String> roles) {
            this.roles = roles;
            return this;
        }

        public UserResourceBuilder roles(String... roles) {
            this.roles = Arrays.asList(roles);
            return this;
        }

        public UserResourceBuilder role(String role) {
            if (this.roles == null) {
                this.roles = new ArrayList<>();
            }
            this.roles.add(role);
            return this;
        }

        public UserResource build() {
            UserResource result = new UserResource();

            result.setId(id);
            result.setUserName(userName);
            result.setFirstName(firstName);
            result.setLastName(lastName);
            result.setEmail(email);
            result.setRoles(roles);

            return result;
        }

    }

}
