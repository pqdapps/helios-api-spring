package mx.esapco.helios.service;

import java.util.List;

import mx.esapco.helios.persistence.app.entity.User;

public interface UserService {
    public List<User> getUsers();

    public User createUser(User user);

    public User findByUsername(String username);
}
