package mx.esapco.helios.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.esapco.helios.persistence.app.entity.Role;
import mx.esapco.helios.persistence.app.entity.User;
import mx.esapco.helios.persistence.app.repository.RoleRepository;
import mx.esapco.helios.persistence.app.repository.UserRepository;
import mx.esapco.helios.security.service.PasswordService;

@Service
@Transactional(transactionManager = "appTransactionManager")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordService passwordService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
            PasswordService passwordService) {
        if (userRepository == null) {
            throw new IllegalArgumentException("userRepository can't be null");
        }
        if (roleRepository == null) {
            throw new IllegalArgumentException("roleRepository can't be null");
        }
        if (passwordService == null) {
            throw new IllegalArgumentException("passwordService can't be null");
        }
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordService = passwordService;
    }

    @Override
    public List<User> getUsers() {
        List<User> result = userRepository.findAll();
        if (result == null) {
            throw new RuntimeException("The user result is empty");
        }
        return result;
    }

    @Override
    @Transactional
    public User createUser(User user) {
        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            Set<Role> requestedRoles = user.getRoles();
            Set<Role> attachedRoles = new HashSet<>(requestedRoles.size());

            user.setPassword(passwordService.encode(user.getPassword()));

            for (Role role : requestedRoles) {
                Role attachedRole = roleRepository.findByRoleName(role.getRoleName());
                attachedRoles.add(attachedRole);
                attachedRole.setUsers(Collections.singleton(user));
            }
            user.setRoles(attachedRoles);

        }
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUserName(username);
    }

}
