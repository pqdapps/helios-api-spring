package mx.esapco.helios.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import mx.esapco.helios.persistence.app.entity.User;
import mx.esapco.helios.persistence.datasource.CachedDataSourceProvider;
import mx.esapco.helios.persistence.datasource.DataSourceProvider;
import mx.esapco.helios.persistence.datasource.MultitenantRoutingDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "mx.esapco.helios.persistence.app.repository" },
        entityManagerFactoryRef = "appEntityManagerFactory",
        transactionManagerRef = "appTransactionManager")
@PropertySource("classpath:application.properties")
public class CompanyJpaConfiguration {

    @Autowired
    private Environment environment;

    @Autowired
    private DataSourceProvider dataSourceProvider;

    @Bean
    public LocalContainerEntityManagerFactoryBean appEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[] { User.class.getPackageName() });
        // em.setPackagesToScan(new String[] { "mx.esapco.helios.persistence.app.entity"
        // });

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(hibernateProperties());

        return em;
    }

    @Bean
    public DataSource dataSource() {
        return new MultitenantRoutingDataSource(new CachedDataSourceProvider(dataSourceProvider),
                companyDataSource());
    }

    @Bean(destroyMethod = "close")
    public DataSource companyDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(environment.getProperty("app.datasource.url"));
        hikariConfig
                .setDriverClassName(environment.getProperty("app.datasource.driver-class-name"));
        hikariConfig.setUsername(environment.getProperty("app.datasource.username"));
        hikariConfig.setPassword(environment.getProperty("app.datasource.password"));
        hikariConfig.setPoolName(environment.getProperty("app.datasource.name"));
        hikariConfig.setAutoCommit(
                environment.getProperty("app.datasource.auto-commit", Boolean.class));
        hikariConfig
                .setConnectionTestQuery(environment.getProperty("app.datasource.validation-query"));
        hikariConfig.setMinimumIdle(
                environment.getProperty("app.datasource.connection-pool.min-idle", Integer.class));
        hikariConfig.setMaximumPoolSize(environment
                .getProperty("app.datasource.connection-pool.max-active", Integer.class));
        hikariConfig
                .setReadOnly(environment.getProperty("app.datasource.read-only", Boolean.class));
        return new HikariDataSource(hikariConfig);
    }

    Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto",
                environment.getProperty("spring.jpa.hibernate.ddl-auto"));
        properties.setProperty("hibernate.dialect",
                environment.getProperty("spring.jpa.hibernate.dialect"));
        properties.setProperty("hibernate.show_sql",
                environment.getProperty("spring.jpa.hibernate.show_sql"));

        return properties;
    }

    @Bean
    public PlatformTransactionManager appTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(appEntityManagerFactory().getObject());
        transactionManager.setDataSource(dataSource());

        return transactionManager;
    }

}
