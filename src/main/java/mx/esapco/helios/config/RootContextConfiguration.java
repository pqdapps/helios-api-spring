package mx.esapco.helios.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "mx.esapco.helios" })
public class RootContextConfiguration {

}
