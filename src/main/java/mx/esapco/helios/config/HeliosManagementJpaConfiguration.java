package mx.esapco.helios.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import mx.esapco.helios.persistence.management.entity.Company;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "mx.esapco.helios.persistence.management.repository" },
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager")
@PropertySource("classpath:application.properties")
public class HeliosManagementJpaConfiguration {

    @Autowired
    private Environment environment;

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(managementDataSource());
        em.setPackagesToScan(new String[] { Company.class.getPackageName() });
        // em.setPackagesToScan(new String[] {
        // "mx.esapco.helios.persistence.management.entity" });

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(hibernateProperties());

        return em;
    }

    @Bean(destroyMethod = "close")
    public DataSource managementDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(environment.getProperty("helios.datasource.url"));
        hikariConfig
                .setDriverClassName(environment.getProperty("helios.datasource.driver-class-name"));
        hikariConfig.setUsername(environment.getProperty("helios.datasource.username"));
        hikariConfig.setPassword(environment.getProperty("helios.datasource.password"));
        hikariConfig.setPoolName(environment.getProperty("helios.datasource.name"));
        hikariConfig.setConnectionTestQuery(
                environment.getProperty("helios.datasource.validation-query"));
        hikariConfig.setMinimumIdle(environment
                .getProperty("helios.datasource.connection-pool.min-idle", Integer.class));
        hikariConfig.setMaximumPoolSize(environment
                .getProperty("helios.datasource.connection-pool.max-idle", Integer.class));
        hikariConfig.setIdleTimeout(environment
                .getProperty("helios.datasource.connection-pool.idle-timeout", Long.class));
        hikariConfig.setConnectionTimeout(
                environment.getProperty("helios.datasource.connection-timeout", Long.class));
        hikariConfig
                .setReadOnly(environment.getProperty("helios.datasource.read-only", Boolean.class));
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    @Primary
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        transactionManager.setDataSource(managementDataSource());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto",
                environment.getProperty("spring.jpa.hibernate.ddl-auto"));
        properties.setProperty("hibernate.dialect",
                environment.getProperty("spring.jpa.hibernate.dialect"));
        properties.setProperty("hibernate.show_sql",
                environment.getProperty("spring.jpa.hibernate.show_sql"));

        return properties;
    }
}
