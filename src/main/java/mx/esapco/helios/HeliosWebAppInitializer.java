package mx.esapco.helios;

/**
 * Clase que permite configurar el ApplicationContext de Spring y registrar el
 * Servlet Dispatcher de Spring para que este se haga cargo de mapear las URLs
 * de los controllers. El usuario tiene mas control sobre la configiuracion del
 * ApplicationContext y el ServletDispatcher. Esta clase es el reemplazo
 * efectivo del archivo web.xml
 * 
 * @author djimenez
 *
 */
public class HeliosWebAppInitializer { // implements WebApplicationInitializer {

    // @Override
    // public void onStartup(ServletContext servletContext) throws ServletException
    // {
    // // Create the 'root' Spring application context
    // AnnotationConfigWebApplicationContext rootContext = new
    // AnnotationConfigWebApplicationContext();
    // rootContext.scan("mx.esapco.helios");
    //
    // // Manage the lifecycle of the root application context
    // servletContext.addListener(new ContextLoaderListener(rootContext));
    //
    // // Create the dispatcher servlet's Spring application context
    // AnnotationConfigWebApplicationContext dispatcherServlet = new
    // AnnotationConfigWebApplicationContext();
    // dispatcherServlet.register(WebMvcConfiguration.class);
    //
    // // Register and map the dispatcher servlet
    // ServletRegistration.Dynamic dispatcher =
    // servletContext.addServlet("dispatcher",
    // new DispatcherServlet(dispatcherServlet));
    // dispatcher.setLoadOnStartup(1);
    // dispatcher.addMapping("/");
    // }

}
