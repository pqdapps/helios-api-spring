package mx.esapco.helios;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import mx.esapco.helios.config.WebMvcConfiguration;
import mx.esapco.helios.security.config.HeliosSecurityConfiguration;

/**
 * Clase que permite configurar el ApplicationContext de Spring basado en
 * anotaciones y registra el Servlet Dispatcher de Spring para que este se haga
 * cargo de mapear las URLs de los controllers. Esta clase es el reemplazo
 * efectivo del archivo web.xml
 * 
 * @author djimenez
 *
 */
public class HeliosSmallWebAppInitializer
        extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { HeliosSecurityConfiguration.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebMvcConfiguration.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

}
