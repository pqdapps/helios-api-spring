package mx.esapco.helios.security.service;

public interface PasswordService {

    public String encode(String password);

    public boolean matches(String rawPassword, String encodedPassword);

}
