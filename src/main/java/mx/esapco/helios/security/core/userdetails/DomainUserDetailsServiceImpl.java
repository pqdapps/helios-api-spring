package mx.esapco.helios.security.core.userdetails;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import mx.esapco.helios.domain.context.DomainContextHolder;
import mx.esapco.helios.persistence.app.entity.Role;
import mx.esapco.helios.persistence.app.entity.User;
import mx.esapco.helios.persistence.app.repository.UserRepository;

@Service
public class DomainUserDetailsServiceImpl implements DomainUserDetailsService {

    protected static final String USERNAME_DOMAIN_SEPARATOR = "@";

    private UserRepository userRepository;

    @Autowired
    public DomainUserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadByUsernameAndDomain(String username, String domain) {
        if (username == null || username.isEmpty() || domain == null || domain.isEmpty()) {
            throw new UsernameNotFoundException("User@domain must be provided");
        }
        DomainContextHolder.setDomain(domain);
        User user = userRepository.findByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        return convertToUserDetails(user, domain);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        if (username == null || username.isEmpty()) {
            throw new UsernameNotFoundException("Username and domain must be provided");
        }
        if (!username.contains(USERNAME_DOMAIN_SEPARATOR)) {
            throw new UsernameNotFoundException("Username does not include the domain");
        }

        String[] userDomain = username.split(USERNAME_DOMAIN_SEPARATOR);
        return loadByUsernameAndDomain(userDomain[0], userDomain[1]);
    }

    private HeliosUserDetails convertToUserDetails(User user, String domain) {
        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        List<GrantedAuthority> authorities = user.getRoles().stream().map(Role::getRoleName)
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());

        return new HeliosUserDetails(user.getUserName(), user.getPassword(), domain,
                user.getEmail(), user.getFirstName(), user.getLastName(), authorities);

    }

}
