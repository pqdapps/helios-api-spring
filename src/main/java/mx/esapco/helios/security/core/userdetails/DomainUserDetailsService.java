package mx.esapco.helios.security.core.userdetails;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface DomainUserDetailsService extends UserDetailsService {

    public UserDetails loadByUsernameAndDomain(String username, String domain);

}
