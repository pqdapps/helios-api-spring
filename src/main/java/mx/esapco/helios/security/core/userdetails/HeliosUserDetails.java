package mx.esapco.helios.security.core.userdetails;

import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class HeliosUserDetails extends User {

    private static final long serialVersionUID = -5030862615551076808L;

    private final String firstName;
    private final String lastName;
    private final String email;
    private final String companyName;

    public HeliosUserDetails(String username, String password, String companyName, String email,
            String firstName, String lastName, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.companyName = companyName;
    }

    public HeliosUserDetails(String username, String password, boolean enabled,
            boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
            Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities);
        this.firstName = null;
        this.lastName = null;
        this.email = null;
        this.companyName = null;
    }

    public HeliosUserDetails(String username, String password,
            Collection<? extends GrantedAuthority> authorities) {
        this(username, password, null, null, null, null, authorities);
    }

    @Override
    public String getUsername() {
        return super.getUsername() + "@" + getCompanyName();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getCompanyName() {
        return companyName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getCompanyName() == null) ? 0 : getCompanyName().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        HeliosUserDetails other = (HeliosUserDetails) obj;
        return Objects.equals(this.getUsername(), other.getUsername())
                && Objects.equals(this.getCompanyName(), other.getCompanyName())
                && Objects.equals(this.getPassword(), other.getPassword());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("HeliosUserDetails[");
        builder.append("username=");
        builder.append(getUsername());
        builder.append(", credentials=[PROTECTED]");
        builder.append(", firstName=");
        builder.append(firstName);
        builder.append(", lastName=");
        builder.append(lastName);
        builder.append(", email=");
        builder.append(email);
        builder.append(", companyName=");
        builder.append(companyName);
        builder.append("]");
        return builder.toString();
    }

}
