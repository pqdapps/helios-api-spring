package mx.esapco.helios.security.authentication;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import mx.esapco.helios.domain.context.DomainContextHolder;
import mx.esapco.helios.persistence.app.entity.Role;
import mx.esapco.helios.persistence.app.entity.User;
import mx.esapco.helios.security.core.userdetails.HeliosUserDetails;
import mx.esapco.helios.security.service.PasswordService;
import mx.esapco.helios.service.UserService;

public class DomainUsernamePasswordAuthenticationProvider
        extends AbstractUserDetailsAuthenticationProvider {

    private final UserService userService;
    private final PasswordService passwordService;

    public DomainUsernamePasswordAuthenticationProvider(UserService userService,
            PasswordService passwordService) {
        this.userService = userService;
        this.passwordService = passwordService;
    }

    @Override
    protected UserDetails retrieveUser(String username,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        String domain = null;
        if (authentication instanceof DomainUsernamePasswordAuthenticationToken) {
            DomainUsernamePasswordAuthenticationToken domainAuthentication = (DomainUsernamePasswordAuthenticationToken) authentication;
            username = domainAuthentication.getName();
            domain = domainAuthentication.getDomain();
        } else {
            String[] userDomain = username.split("@");
            if (userDomain.length == 2) {
                username = userDomain[0];
                domain = userDomain[1];
            }
        }
        if (domain == null) {
            throw new UsernameNotFoundException("The domain parameter is missing");
        }

        DomainContextHolder.setDomain(domain);
        User user = null;
        try {
            user = userService.findByUsername(username);
        } finally {
            DomainContextHolder.clearContext();
        }

        if (user == null) {
            throw new UsernameNotFoundException("Invalid username/password");
        }
        HeliosUserDetails userDetails = convertToUserDetails(user, domain);
        return userDetails;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (!passwordService.matches((String) authentication.getCredentials(),
                userDetails.getPassword())) {
            throw new BadCredentialsException("Invalid username/password");
        }
    }

    private static HeliosUserDetails convertToUserDetails(User user, String domain) {
        Collection<GrantedAuthority> authorities = new HashSet<>();
        for (Role role : user.getRoles()) {
            GrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
            authorities.add(authority);
        }
        return new HeliosUserDetails(user.getUserName(), user.getPassword(), domain,
                user.getEmail(), user.getFirstName(), user.getLastName(), authorities);

    }

}
