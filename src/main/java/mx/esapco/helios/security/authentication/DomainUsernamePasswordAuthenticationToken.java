package mx.esapco.helios.security.authentication;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import mx.esapco.helios.security.core.userdetails.HeliosUserDetails;

public class DomainUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private static final long serialVersionUID = -8540870996043765065L;

    private final String domain;

    public DomainUsernamePasswordAuthenticationToken(Object principal, Object credentials,
            String domain, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.domain = domain;
    }

    public DomainUsernamePasswordAuthenticationToken(Object principal, Object credentials,
            String domain) {
        super(principal, credentials);
        this.domain = domain;
    }

    public DomainUsernamePasswordAuthenticationToken(HeliosUserDetails principal) {
        this(principal, principal.getPassword(), principal.getCompanyName(),
                principal.getAuthorities());
    }

    public String getDomain() {
        return domain;
    }

}
