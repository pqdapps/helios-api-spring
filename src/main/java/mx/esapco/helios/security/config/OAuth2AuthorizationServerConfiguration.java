package mx.esapco.helios.security.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import mx.esapco.helios.security.core.userdetails.DomainUserDetailsService;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    private ClientDetailsService clientDetailsService;

    @Autowired
    private DomainUserDetailsService userDetailsService;

    @Autowired
    private Environment environment;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()")
                .realm(environment.getProperty("oauth2.authorization-server.realm"));
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // @formatter:off
        endpoints.tokenStore(tokenStore())
            .userApprovalHandler(userApprovalHandler())
            .authenticationManager(authenticationManager)
            .authorizationCodeServices(authorizationCodeServices())
            .accessTokenConverter(defaultAccessTokenConverter())
//            .accessTokenConverter(jwtAccessTokenConverter())
            .userDetailsService(userDetailsService); // Used when refreshing a token
        // @formatter:on

    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // @formatter:off
//        clientDetailsService = clients.jdbc(oauthDataSource()).build();
        clients.inMemory()
            .withClient("my-trusted-client")
            .secret("trusted") 
            .authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit", "client_credentials")
            .authorities("ROLE_TRUSTED_CLIENT").scopes("read", "write")
            .redirectUris("http://localhost:8080/helios-app-spring/oauth/callback")
            .accessTokenValiditySeconds(300).refreshTokenValiditySeconds(600)
            .autoApprove(true)
        .and()
            .withClient("my-client")
            .secret("secret")
            .authorizedGrantTypes("authorization_code", "refresh_token", "implicit")
            .authorities("ROLE_CLIENT").scopes("read")
            .accessTokenValiditySeconds(300).refreshTokenValiditySeconds(600)
            .autoApprove(false);
        // @formatter:on
    }

    @Bean
    @Primary
    public TokenStore tokenStore() {
        return new JdbcTokenStore(oauthDataSource());
        // return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public DefaultAccessTokenConverter defaultAccessTokenConverter() {
        return new DefaultAccessTokenConverter();
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(
                environment.getProperty("oauth2.authorization-server.jwt.secret-key"));
        return jwtAccessTokenConverter;
    }

    @Bean
    public TokenStoreUserApprovalHandler userApprovalHandler() {
        TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
        handler.setTokenStore(tokenStore());
        handler.setRequestFactory(new DefaultOAuth2RequestFactory(clientDetailsService));
        handler.setClientDetailsService(clientDetailsService);
        return handler;
    }

    @Bean
    public OAuth2AuthenticationEntryPoint oauthAuthenticationEntryPoint() {
        OAuth2AuthenticationEntryPoint entryPoint = new OAuth2AuthenticationEntryPoint();
        entryPoint.setRealmName("helios/oauth");
        return entryPoint;
    }

    @Bean
    public OAuth2AuthenticationEntryPoint clientAuthenticationEntryPoint() {
        OAuth2AuthenticationEntryPoint entryPoint = new OAuth2AuthenticationEntryPoint();
        entryPoint.setRealmName("helios/oauthClient");
        entryPoint.setTypeName("Basic");
        return entryPoint;
    }

    @Bean
    public ClientCredentialsTokenEndpointFilter clientCredentialsTokenEndpointFilter() {
        ClientCredentialsTokenEndpointFilter filter = new ClientCredentialsTokenEndpointFilter();
        filter.setAuthenticationManager(authenticationManager);

        return filter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setReuseRefreshToken(true);
        defaultTokenServices.setAccessTokenValiditySeconds(600);
        defaultTokenServices.setAuthenticationManager(authenticationManager);
        defaultTokenServices.setClientDetailsService(clientDetailsService);
        return defaultTokenServices;
    }

    @Bean
    public AuthorizationCodeServices authorizationCodeServices() {
        JdbcAuthorizationCodeServices authCodeServices = new JdbcAuthorizationCodeServices(
                oauthDataSource());
        return authCodeServices;

    }

    @Bean(destroyMethod = "close")
    public DataSource oauthDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(environment.getProperty("oauth.datasource.url"));
        hikariConfig
                .setDriverClassName(environment.getProperty("oauth.datasource.driver-class-name"));
        hikariConfig.setUsername(environment.getProperty("oauth.datasource.username"));
        hikariConfig.setPassword(environment.getProperty("oauth.datasource.password"));
        hikariConfig.setPoolName(environment.getProperty("oauth.datasource.name"));
        hikariConfig.setConnectionTestQuery(
                environment.getProperty("oauth.datasource.validation-query"));
        hikariConfig.setMinimumIdle(environment
                .getProperty("oauth.datasource.connection-pool.min-idle", Integer.class));
        hikariConfig.setMaximumPoolSize(environment
                .getProperty("oauth.datasource.connection-pool.max-idle", Integer.class));
        hikariConfig.setIdleTimeout(environment
                .getProperty("oauth.datasource.connection-pool.idle-timeout", Long.class));
        hikariConfig.setConnectionTimeout(
                environment.getProperty("oauth.datasource.connection-timeout", Long.class));
        hikariConfig
                .setReadOnly(environment.getProperty("oauth.datasource.read-only", Boolean.class));
        return new HikariDataSource(hikariConfig);
    }

}
