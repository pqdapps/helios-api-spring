package mx.esapco.helios.security.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private Environment environment;

    @Autowired
    private DataSource oauthDataSource;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        // @formatter:off
        resources.resourceId(environment.getProperty("oauth2.resource-server.resource-id)"));
        resources.tokenStore(tokenStore());
        resources.stateless(false);
        // @formatter:on

    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
//      // @formatter:off
        http.anonymous().disable()
            .requestMatchers().antMatchers("/users/**")
            .and()
                .authorizeRequests().antMatchers("/users/**").access("hasAuthority('ADMIN')")
            .and()
                .exceptionHandling().accessDeniedHandler(oauthAccessDeniedHandler());
        // @formatter:on

    }

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(oauthDataSource);
        // return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public ResourceServerTokenServices resourceServerTokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore());
        return tokenServices;
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(
                environment.getProperty("oauth2.authorization-server.jwt.secret-key"));
        return jwtAccessTokenConverter;
    }

    @Bean
    public OAuth2AccessDeniedHandler oauthAccessDeniedHandler() {
        OAuth2AccessDeniedHandler handler = new OAuth2AccessDeniedHandler();
        return handler;
    }
}
