package mx.esapco.helios.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.GenericFilterBean;

import mx.esapco.helios.security.authentication.DomainUsernamePasswordAuthenticationProvider;
import mx.esapco.helios.security.service.PasswordService;
import mx.esapco.helios.security.web.DomainContextHolderFilter;
import mx.esapco.helios.security.web.DomainUsernamePasswordAuthenticationFilter;
import mx.esapco.helios.security.web.RestAuthenticationEntryPoint;
import mx.esapco.helios.service.UserService;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "mx.esapco.helios.controller", "mx.esapco.helios.config",
        "mx.esapco.helios.service", "mx.esapco.helios.persistence.app.repository",
        "mx.esapco.helios.persistence.management.repository" })
@ComponentScan(basePackages = { "mx.esapco.helios" })
public class HeliosSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    public UserService userService;

    @Autowired
    public PasswordService passwordService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      // @formatter:off
      http
          .csrf().disable()
          .httpBasic()
      .and()
          .authorizeRequests().antMatchers("/oauth/**").permitAll()
      .and()
          .authorizeRequests().anyRequest().authenticated()
      .and()
          .addFilterAfter(domainContextHolderFilter(), UsernamePasswordAuthenticationFilter.class);
        // @formatter:on
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/login**", "/logout**", "/authenticate");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(domainUsernamePasswordAuthenticationProvider());
    }

    @Bean
    public AuthenticationProvider domainUsernamePasswordAuthenticationProvider() {
        return new DomainUsernamePasswordAuthenticationProvider(userService, passwordService);
    }

    @Bean
    public AuthenticationEntryPoint restAuthenticationEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(8);
    }

    @Bean
    public GenericFilterBean domainContextHolderFilter() {
        return new DomainContextHolderFilter();
    }

    @Bean
    public GenericFilterBean domainUsernamePasswordAuthenticationFilter() throws Exception {
        DomainUsernamePasswordAuthenticationFilter filter = new DomainUsernamePasswordAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManagerBean());

        return filter;

    }

}
