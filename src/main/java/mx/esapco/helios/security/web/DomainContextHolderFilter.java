package mx.esapco.helios.security.web;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

import mx.esapco.helios.domain.context.DomainContextHolder;
import mx.esapco.helios.security.core.userdetails.HeliosUserDetails;

public class DomainContextHolderFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Principal principal = httpRequest.getUserPrincipal();

        if (principal instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) principal;
            if (token.getPrincipal() instanceof HeliosUserDetails) {
                HeliosUserDetails userPrincipal = (HeliosUserDetails) token.getPrincipal();
                DomainContextHolder.setDomain(userPrincipal.getCompanyName());
            }
        }

        chain.doFilter(httpRequest, response);
    }

}
