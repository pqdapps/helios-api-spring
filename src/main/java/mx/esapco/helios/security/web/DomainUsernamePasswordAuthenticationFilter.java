package mx.esapco.helios.security.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import mx.esapco.helios.security.authentication.DomainUsernamePasswordAuthenticationToken;

public class DomainUsernamePasswordAuthenticationFilter
        extends UsernamePasswordAuthenticationFilter {

    public static final String SPRING_SECURITY_FORM_DOMAIN_KEY = "domain";

    private String domainParameter = SPRING_SECURITY_FORM_DOMAIN_KEY;

    public DomainUsernamePasswordAuthenticationFilter() {
        super();
    }

    protected String obtainDomain(HttpServletRequest request) {
        return request.getParameter(domainParameter);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
            HttpServletResponse response) throws AuthenticationException {

        DomainUsernamePasswordAuthenticationToken authRequest = getAuthRequest(request);
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private DomainUsernamePasswordAuthenticationToken getAuthRequest(HttpServletRequest request) {
        String username = obtainUsername(request);
        String password = obtainPassword(request);
        String domain = obtainDomain(request);

        if (username == null) {
            username = "";
        }
        if (password == null) {
            password = "";
        }
        if (domain == null) {
            domain = "";
        }

        return new DomainUsernamePasswordAuthenticationToken(username, password, domain);
    }

    public String getDomainParameter() {
        return domainParameter;
    }

    public void setDomainParameter(String domainParameter) {
        this.domainParameter = domainParameter;
    }

}