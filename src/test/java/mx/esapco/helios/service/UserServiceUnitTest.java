package mx.esapco.helios.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import mx.esapco.helios.persistence.app.entity.User;
import mx.esapco.helios.persistence.app.repository.RoleRepository;
import mx.esapco.helios.persistence.app.repository.UserRepository;
import mx.esapco.helios.security.service.PasswordService;

public class UserServiceUnitTest {

    private UserRepository userRepositoryMock;
    private RoleRepository roleRepositoryMock;
    private PasswordService passwordServiceMock;

    private List<User> usersResult;

    // Target class
    private UserServiceImpl userService;

    @Before
    public void setUp() {
        userRepositoryMock = Mockito.mock(UserRepository.class);
        roleRepositoryMock = Mockito.mock(RoleRepository.class);
        passwordServiceMock = Mockito.mock(PasswordService.class);
        userService = new UserServiceImpl(userRepositoryMock, roleRepositoryMock,
                passwordServiceMock);
        usersResult = new ArrayList<>();
        User user1 = new User();
        user1.setEmail("test@test.com");
        user1.setFirstName("Test");
        user1.setLastName("Test");
        user1.setUserName("testUserName");
        user1.setUserId(1L);

        User user2 = new User();
        user2.setEmail("test2@test.com");
        user2.setFirstName("Test 2");
        user2.setLastName("Test 2");
        user2.setUserName("testUserName 2");
        user2.setUserId(2L);

        usersResult.add(user1);
        usersResult.add(user2);
    }

    @Test(expected = RuntimeException.class)
    public void testGetUsersAreNull() {
        Mockito.when(userRepositoryMock.findAll()).thenReturn(null);
        List<User> result = userService.getUsers();
        Mockito.verify(userRepositoryMock, Mockito.times(1)).findAll();
    }

    @Test
    public void testGetUsersAreNotNull() {
        Mockito.when(userRepositoryMock.findAll()).thenReturn(usersResult);
        List<User> result = userService.getUsers();
        Mockito.verify(userRepositoryMock, Mockito.times(1)).findAll();
        assertNotNull(result);
        assertTrue(result.size() == 2);
        // assertTrue(result.get(0).getUserId() == 1L);
    }

    @After
    public void tearDown() {
        usersResult = null;
        userRepositoryMock = null;
        userService = null;
    }

}
